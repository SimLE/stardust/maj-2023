#ifdef CUTDOWN

void SetupCutdown(void)
{
    digitalWrite(CUTDOWN, 0);
    pinMode(CUTDOWN, OUTPUT);
    digitalWrite(CUTDOWN, 0);
    Serial.println("CUTDOWN READY");
}

void CutdownNow(void)
{
  Serial.println("CUTDOWN ON");
  digitalWrite(CUTDOWN, 1);
  delay(10000);
  digitalWrite(CUTDOWN, 0);
  Serial.println("CUTDOWN OFF");
}

void CheckCutdown(void)
{
  // Don't do anything unless we have GPS
  if (GPS.Satellites >= 4)
  {
    // Arm ?
    if ((GPS.Altitude >= 0) && (GPS.CutdownStatus == 0))
    {
      GPS.CutdownStatus = 1;      // Armed
      Serial.println("GPS Armed");
    }

    // Trigger only if armed
    if (GPS.CutdownStatus == 1)
    {
      // Uncomment/modify the following code to trigger the cutdown appropriately for your flight
    
      // ALTITUDE TEST
      
      if (GPS.Altitude > 32000)
      {
        GPS.CutdownStatus = 2;      // Altitude trigger
        CutdownNow();
      }
     
    
      // LONGITUDE TEST
      //if (GPS.Latitude >= 0.0)
      if (GPS.Latitude > 54.0 || GPS.Longitude > 22.0)
      {
        GPS.CutdownStatus = 3;      // Longitude trigger
        CutdownNow();
      }
      
    }
  }
}

#endif
