#include <Arduino.h>
#include <Servo.h>


/* SimLE Stardust / Odcinacz
 by yasiu <http://y4s.io>

  Pilot -> Odbiornik -> Nano
  A -> D2 -> D6
  B -> D0 -> D8
  C -> D3 / NC
  D -> D1 / NC
  D0 ->D8



*/

#define RADIO_OPEN_PIN 6
#define RADIO_CLOSE_PIN 8
#define SERVO_PIN 12
#define BUZZER_PIN 13
#define SERVO_OPEN 90
#define SERVO_CLOSE 119
#define OPEN 1
#define CLOSED 0

Servo myservo;
uint8_t state = 0;

void setup() {
  Serial.begin(9600);
  myservo.attach(SERVO_PIN);
  myservo.write(SERVO_CLOSE);

  pinMode(RADIO_OPEN_PIN, INPUT);
  pinMode(RADIO_CLOSE_PIN, INPUT);
  pinMode(BUZZER_PIN, OUTPUT);
}

void loop() {
  if(digitalRead(RADIO_OPEN_PIN) == HIGH) {
    digitalWrite(BUZZER_PIN, HIGH);
    state=OPEN;
    myservo.write(SERVO_OPEN);
    Serial.println(state);
  }

  if(digitalRead(RADIO_CLOSE_PIN) == HIGH) {
    digitalWrite(BUZZER_PIN, LOW);
    state=CLOSED;
    myservo.write(SERVO_CLOSE);
    Serial.println(state);
  }
  
}